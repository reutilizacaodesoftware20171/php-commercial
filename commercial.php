<?php 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Headers: Content-Type"); ?> <?php

require_once('settings.php');

$query = $_GET["query"];
$tweets = getTweetsWith($query);

$commercial = array();
foreach ($tweets as &$tweet) {
	$result = $DatumboxAPI->CommercialDetection($tweet);
	if (!isset($commercial[$result])) {
	    $commercial[$result] = 0;
	}
	$commercial[$result]++;
}

unset($DatumboxAPI);

print json_encode($commercial);